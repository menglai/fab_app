const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const cors = require('cors');

const dbConfig = require ('./dbConfig');
const pool = mysql.createPool(dbConfig);

const app = express();

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const PORT = parseInt(process.argv[2]) || process.env.APP_PORT || 3000;


const SQL_INSERT = 'INSERT INTO food_diary(diary_id, breakfast, lunch, dinner, supper, snacks, comments, updated_date) values (?, ?, ?, ?, ?, ?, ?, ?)';


app.post('/diary/save', (req, resp) => {
    const params = [req.body.diary_id, req.body.breakfast, req.body.lunch, req.body.dinner, req.body.supper, req.body.snacks, req.body.comments, req.body.updated_date];
    pool.getConnection((err, conn) => {
        if (err) {
            console.error('>> error: ', err);
            resp.status(500).json({error: err});
            return;
        }
        try {
            conn.query(SQL_INSERT, params,
                (err, results) => {
                    if (err) {
                        resp.status(400).json({error: err});
                        return;
                    }
                    else {
                        resp.status (201).json({});
                    }
                });
        } finally {
            conn.release();
        }
    })
})

const SQL_DISPLAY = 'select diary_id, breakfast, lunch, dinner, supper, snacks, comments, updated_date from food_diary order by diary_id desc limit ?';

app.get ('/diary', (req, resp) =>{
    const limit = req.query.limit || 8;

    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SQL_DISPLAY, [ parseInt(limit) ],
            (error, results) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error}); return;
                    }
                    resp.status(200).json(results);
                } finally {
                    conn.release();
                }
            }
        )
    });
 });

 const deleteDiary = 'DELETE FROM food_diary WHERE diary_id = ?'

 app.delete('/diary/:delId', (req, resp) => {
     console.log(req.params.delId);
    const params = [req.params.delId];
    pool.getConnection((err, conn) => {
        if (err) {
            console.error('>> error: ', err);
            resp.status(500).json({error: err});
            return;
        }
        try {
            conn.query(deleteDiary, params,
                (err, results) => {
                    if (err) {
                        resp.status(400).json({error: err});
                        return;
                    }
                    else {
                        resp.status (201).json({});
                    }
                });
        } finally {
            conn.release();
        }
    })
})

const SQL_UPDATE = 'UPDATE food_diary SET breakfast =?, lunch = ?, dinner = ?, supper = ?, comments = ?, updated_date=?, diary_id=?';

app.put("/diary", function (req, resp) {
    console.log(req.body);
    const params = [req.body.diary_id, req.body.breakfast, req.body.lunch, req.body.dinner, req.body.supper, req.body.comments, req.body.updated_date];
    pool.getConnection((err, conn) => {
        if (err) {
            console.error('>> error: ', err);
            resp.status(500).json({error: err});
            return;
        }
        try {
            conn.query(SQL_UPDATE, params,
                (err, results) => {
                    if (err) {
                        resp.status(400).json({error: err});
                        return;
                    }
                    else {
                        resp.status (201).json({});
                    }
                });
        } finally {
            conn.release();
        }
    })
});


console.log('Pinging database...');

pool.getConnection((err, conn) => {
    if (err) {
        console.error('>Error: ', err);
        process.exit(-1);
    }

    conn.ping((err) => {
        if (err) {
            console.error('>Cannot ping: ', err);
            process.exit(-1);
        }
        conn.release();

        //Start application only if we can ping database
        app.listen(PORT, () => {
            console.log('Starting application on port %d', PORT);
        });
    });
});

app.use(express.static(__dirname + "/public"));