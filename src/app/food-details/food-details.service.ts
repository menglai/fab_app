import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable} from 'rxjs/Rx';
import { environment } from '../../environments/environment';

import { DiaryM } from '../model/diary.model';
import { FabDiaryService } from '../fab-diary/fabDiary.service';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const httpOptions = { 
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class FoodDetailsService {

  private diaryRootApiUrl  = `${environment.ApiUrl}/diary`;

  constructor(private httpClient: HttpClient){}

  public updateDiary (diary: DiaryM): Observable<DiaryM> {
    console.log(diary);
    console.log(this.diaryRootApiUrl);
    return this.httpClient.put<DiaryM>(this.diaryRootApiUrl, diary, httpOptions)
      .pipe(
        catchError(this.handleError('updateDiary', diary))
      );
  }

    public deleteBook (diaryId: number) {
      console.log(diaryId);
      console.log(this.diaryRootApiUrl);
      const deleteUrl = `${this.diaryRootApiUrl}/${diaryId}`;
      console.log(deleteUrl);
      return this.httpClient.delete(deleteUrl, httpOptions)
        .pipe(
          catchError(this.handleError('deleteDiary', diaryId))
        );

      //return null;
    }

    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
        return Observable.throw(error  || 'backend server error');
      };
    }
    
  }