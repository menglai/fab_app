import { Component, OnInit, TemplateRef } from '@angular/core';
import { FabDiaryService } from '../fab-diary/fabDiary.service';
import { FoodDetailsService } from '../food-details/food-details.service'
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { Observable} from 'rxjs/Rx';

import { DiaryM } from '../model/diary.model'
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-food-details',
  templateUrl: './food-details.component.html',
  styleUrls: ['./food-details.component.css']
})
export class FoodDetailsComponent implements OnInit {

  deleteDiaryObservable$: Observable<any>;
  updateDiaryObservable$: Observable<DiaryM>;

  diaryDetails : any = [];
  private limit = 8;
  editDetail: boolean = false;
  editId: number;
  results = [];
  private editDairy: DiaryM;
  modalRef: BsModalRef;
  

  constructor(
    private diarySvc: FabDiaryService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private httpClient: HttpClient,
    private foodDetailsServ: FoodDetailsService,
    private modalService: BsModalService) {}

  ngOnInit() {
  this.loadDiary();
  this.diaryDetails
  
}

private loadDiary() {
  this.diarySvc.getAllDiary({limit: this.limit})
  .then((results) => { 
    this.diaryDetails = results;
    console.log("results > " + results);
  })
  .catch((error) => { console.log(error)})
}

edit(d: DiaryM, index: number, template: TemplateRef<any>){
  console.log('<<< >>>');
  console.log('<<<' + d.breakfast);
  this.editDairy = new DiaryM (
    d.breakfast, d.lunch, d.dinner, d.supper,
    d.comments, d.updated_date, d.diary_id);
  this.modalRef = this.modalService.show(template);
}

updateD(index: number){
console.log(index);
  this.updateDiaryObservable$ = this.foodDetailsServ.updateDiary(this.editDairy);
  this.updateDiaryObservable$.subscribe(diary => {
    var diaryUpdate = this.results[index];
    diaryUpdate.breakfast = this.editDairy.breakfast;
    diaryUpdate.lunch = this.editDairy.lunch;
    diaryUpdate.dinner = this.editDairy.dinner;
    diaryUpdate.supper = this.editDairy.supper;
    diaryUpdate.comments = this.editDairy.comments;
    diaryUpdate.diary_id = this.editDairy.diary_id;
    diaryUpdate.updated_date = this.editDairy.updated_date;
    this.results[index] = diaryUpdate;
    this.modalRef.hide();
  })

}

delete(diaryDetails, index: number){
console.log('delete diary' + index);
this.deleteDiaryObservable$ = this.foodDetailsServ.deleteBook(diaryDetails.diary_id);
this.deleteDiaryObservable$.subscribe(diary => {
  var deleteD = this.results[index];
  var index = this.results.indexOf(deleteD, 0);
  if (index > -1){
    return this.results.splice(index, 1)
  }
  console.log(this.diaryDetails[0])
})
this.diaryDetails.splice (index);

}

}

