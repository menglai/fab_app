import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FabDiaryService } from './fabDiary.service';


@Component({
  selector: 'app-fab-diary',
  templateUrl: './fab-diary.component.html',
  styleUrls: ['./fab-diary.component.css']
})
export class FabDiaryComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;

  constructor(private diarySvc: FabDiaryService) { 
    this.datePickerConfig = Object.assign({}, 
      { containerClass: 'theme-orange',
        showWeekNumbers: true,
        minDate: new Date (2018, 0, 1),
        dateInputFormat: 'DD/MM/YYYY',

    });
}

  ngOnInit() {
  
  }

  saveDiary(dForm: NgForm) {
    console.log(dForm.value);
    const d = new Date(dForm.value.dDate)
    const month = d.getMonth() + 1;
    const dateStr = d.getDate() + '-' + month + '-' + d.getFullYear();
    console.log('date = ', dateStr );

    this.diarySvc.saveDiary ({
      updated_date: dateStr,
      breakfast: dForm.value.breakfast,
      lunch: dForm.value.lunch,
      dinner: dForm.value.dinner,
      supper: dForm.value.supper,
      snacks: dForm.value.snacks,
      comments: dForm.value.comments,
    })
    .then(() => {console.log ('saved!');})
    .catch(error => {console.error ('error', error);})
    dForm.reset();
  }



}
