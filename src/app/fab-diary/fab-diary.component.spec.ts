import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabDiaryComponent } from './fab-diary.component';

describe('FabDiaryComponent', () => {
  let component: FabDiaryComponent;
  let fixture: ComponentFixture<FabDiaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabDiaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabDiaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
