import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { FoodDetailsService } from '../food-details/food-details.service'
import { catchError } from 'rxjs/operators';
import { Observable} from 'rxjs/Rx';
import { DiaryM } from '../model/diary.model';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const httpOptions = { 
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class FabDiaryService {



  diarySubject = new Subject();

  constructor(private httpClient: HttpClient) { }

  saveDiary(diary: any): Promise<any> {
    return (
      this.httpClient.post('http://localhost:3000/diary/save', diary)
        .take(1).toPromise()
        .then(() => { 
        })
      );
  }

  getAllDiary(config = {}): Promise<DiaryM> {
    let qs = new HttpParams();
    qs = qs.set ('limit', config ['limit'] || 7)

    return (
      this.httpClient.get<DiaryM>('http://localhost:3000/diary', { params: qs}).toPromise());
  }

  private handleError ( error: Response | any ){
    console.log ('FabDiaryService:: handleError', error)
    return Observable.throw(error)
  }

}