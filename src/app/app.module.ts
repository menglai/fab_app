import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule, BsDatepickerModule } from 'ngx-bootstrap';
import { FabDiaryService } from './fab-diary/fabDiary.service';
import { FoodDetailsService } from './food-details/food-details.service';

import { AppComponent } from './app.component';
import { FabDiaryComponent } from './fab-diary/fab-diary.component';
import { AppRoutingModule } from './/app-routing.module';
import { FooterComponent } from './footer/footer.component';
import { FoodDetailsComponent } from './food-details/food-details.component';
import { LoginComponent } from './login/login.component';
import { SearchDiaryComponent } from './search-diary/search-diary.component';
import { BsModalService } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    AppComponent,
    FabDiaryComponent,
    FooterComponent,
    FoodDetailsComponent,
    LoginComponent,
    SearchDiaryComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [FabDiaryService, FoodDetailsService, BsModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
