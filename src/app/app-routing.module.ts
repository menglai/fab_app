import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FabDiaryComponent} from './fab-diary/fab-diary.component';
import { FoodDetailsComponent } from './food-details/food-details.component';
import { LoginComponent } from './login/login.component';
import { SearchDiaryComponent} from './search-diary/search-diary.component'


const routes: Routes = [
  {path: 'food-diary', component: FabDiaryComponent, },
  {path: 'food-details', component: FoodDetailsComponent, },
  {path: 'login', component: LoginComponent },
  {path: 'SearchDiary', component: SearchDiaryComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [FabDiaryComponent, FoodDetailsComponent, LoginComponent, SearchDiaryComponent]