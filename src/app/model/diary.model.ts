export class DiaryM {
    constructor(
        public breakfast: string,
        public lunch: string,
        public dinner: string,
        public supper: string,
        public comments: string,
        public updated_date: string,
        public diary_id: number
    ){

    }
}
